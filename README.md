<!-- vim:tw=100:sw=2:ts=2:sts=2:et:cc=100 -->

Sometime in the last year or so, the NGA updated their website and deleted a number of documents
that we depend on as references. This project hosts those files, mostly rescued from [wayback
machine](https://web.archive.org), on [gitlab](https://gitlab.com/is4s/nga-rescue) and [the
web](https://nga-rescue.is4s.us).

We are not affiliated with the National Geospatial-Intelligence Agency. These files were originally
published with unlimited distribution in the public domain. This page and its source code are made
available &copy; 2021 [IS4S, Inc](https://is4s.com) under the [Creative Commons
Attribution](https://creativecommons.org/licenses/by/3.0/) license.

Please [let us know](https://gitlab.com/is4s/nga-rescue/-/issues) if any of these files are still
available from official sources and we'll replace our copy with a web redirect to the official
version.


# Files Included

| File                    | Description                               | Dead Link  | Rescued From  |
|-------------------------|-------------------------------------------|:----------:|:-------:|
|[`wgs84fin.pdf`][wgs-our]| DoD World Geodetic System '84 definition  |[wgs-old]|[wgs-src]|
|[`NGA.STND.0036_1.0.0_WGS84.pdf`][stnd-our] | NGA STANDARDIZATION DOCUMENT  |[stnd-old]|[stnd-src]|
|[`Sections 1-5.pdf`][s15-our] | DMA TECHNICAL REPORT TR8350.2-b - Supplement to DoD WGS 84 Technical Report Part 2 |[s15-old]|[s15-src]|
|[`README_FIRST_new.pdf`][egm-our]| Description of EGM2008 gravity files  |[egm-old]|[egm-src]|
|multiple (see below)|Gold Data v6.3 Testing for Datum Transformations and Coordinate Conversion Software|[gld-old]|[gld-src][gld-src]|

## Gold Data v6.3 Testing for Datum Transformations and Coordinate Conversion Software

These files were rescued from a [wayback machine page][gld-src]. That page includes javascript which
will attempt to redirect you to NGA's new homepage (which is apparently missing these files), and is
only viewable if you turn your browser's Javascript feature off.

* [INSTRUCTIONS](https://nga-rescue.is4s.us/gold/Instructions.pdf)
* [RELEASE NOTES](https://nga-rescue.is4s.us/gold/Release_Notes.pdf)
* [GEOTRANS FILE FORMAT SUMMARY](https://nga-rescue.is4s.us/gold/GeoTrans_File_Format_Summary.pdf)
* [GOLD DATA v6.3](https://nga-rescue.is4s.us/gold/GoldData_v6.3.zip) (1.11 MB)


  [wgs-our]: https://nga-rescue.is4s.us/wgs84fin.pdf
  [wgs-old]: https://earth-info.nga.mil/GandG/publications/tr8350.2/wgs84fin.pdf
  [wgs-src]: http://geodesy.unr.edu/hanspeterplag/library/geodesy/wgs84fin.pdf
  [stnd-our]: https://nga-rescue.is4s.us/NGA.STND.0036_1.0.0_WGS84.pdf
  [stnd-old]: https://earth-info.nga.mil/php/download.php?file=coord-wgs84
  [stnd-src]: https://earth-info.nga.mil/php/download.php?file=coord-wgs84
  [s15-our]: https://nga-rescue.is4s.us/Sections%201-5.pdf
  [s15-old]: http://earth-info.nga.mil/GandG/publications/tr8350.2/TR8350.2-b/Sections%201-5.pdf
  [s15-src]: http://web.archive.org/web/20170204001944/http://earth-info.nga.mil/GandG/publications/tr8350.2/TR8350.2-b/Sections%201-5.pdf
  [egm-our]: https://nga-rescue.is4s.us/egm-readme.pdf
  [egm-old]: https://earth-info.nga.mil/GandG/wgs84/gravitymod/egm2008/Anomaly_DOV/README_FIRST_new.pdf
  [egm-src]: http://web.archive.org/web/20181222100537/https://earth-info.nga.mil/GandG/wgs84/gravitymod/egm2008/Anomaly_DOV/README_FIRST_new.pdf
  [gld-old]: http://earth-info.nga.mil/GandG/coordsys/Conversion_Software/index.html
  [gld-src]: http://web.archive.org/web/20190519231145/http://earth-info.nga.mil/GandG/coordsys/Conversion_Software/index.html
